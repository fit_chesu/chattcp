﻿using System;
using System.Net.Sockets;
using System.Text;

namespace ChatTcp
{
    public class ClientObject
    {
        public TcpClient client;
        public delegate void NewMsgAction(string msg);
        public event NewMsgAction NewMsgEvent;
        static NetworkStream stream;
        bool isWork;
        public ClientObject(TcpClient tcpClient)
        {
            client = tcpClient;
            isWork = true;
        }

        public void Process()
        {
            stream = null;
            try
            {
                stream = client.GetStream();
                byte[] data = new byte[64]; // буфер для получаемых данных
                while (isWork)
                {
                    // получаем сообщение
                    StringBuilder builder = new StringBuilder();
                    int bytes = 0;
                    do
                    {
                        bytes = stream.Read(data, 0, data.Length);
                        builder.Append(Encoding.Unicode.GetString(data, 0, bytes));
                    }
                    while (stream.DataAvailable);

                    string message = builder.ToString();
                    if (NewMsgEvent != null)
                        NewMsgEvent(message);
                    if (message.Equals("Disconnect!") == true || message == "")
                    {
                        SendMessage("Server closed connection!");
                        isWork = false;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                if (NewMsgEvent != null)
                    NewMsgEvent(ex.Message);
            }
            finally
            {
                if (stream != null)
                    stream.Close();
                if (client != null)
                    client.Close();
            }
        }

        public void SendMessage(string msg)
        {
            // преобразуем сообщение в массив байтов
            byte[] data = Encoding.Unicode.GetBytes($"{msg}");
            // отправка сообщения
            stream.Write(data, 0, data.Length);
        }
        public void DisconnectSession()
        {
            isWork = false;
            SendMessage("Disconnect!");
        }
    }
}