﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;


namespace ChatTcp
{
    class Server
    {
        TcpListener listener;
        public delegate void NewMsgAction(string msg);
        public event NewMsgAction NewMsgEvent;
        ClientObject clientObject;
        int PORT;

        public Server(int _PORT)
        {
            PORT = _PORT;
        }
        public void StartServer()
        {
            Thread serverThread = new Thread(new ThreadStart(() =>
            {
                try
                {
                    listener = new TcpListener(IPAddress.Any, PORT);
                    listener.Start();
                    Console.WriteLine("Ожидание подключений...");

                    while (true)
                    {
                        TcpClient client = listener.AcceptTcpClient();
                        clientObject = new ClientObject(client);
                        clientObject.NewMsgEvent += OnNewMessage;

                        // создаем новый поток для обслуживания нового клиента
                        Thread clientThread = new Thread(new ThreadStart(clientObject.Process));
                        clientThread.Start();
                    }
                }
                catch (Exception ex)
                {
                    if (NewMsgEvent != null)
                        NewMsgEvent(ex.Message);
                }
                finally
                {
                    if (listener != null)
                        listener.Stop();
                }
            }));
            serverThread.Start();
        }
        void OnNewMessage(string msg)
        {
            if (NewMsgEvent != null)
                NewMsgEvent(msg);
        }
        public void DisconnectClient()
        {
            try
            {
                if (clientObject != null)
                {
                    clientObject.DisconnectSession();
                    clientObject = null;
                    if (NewMsgEvent != null)
                        NewMsgEvent("Соединение завершено!");
                }
            }
            catch (Exception ex)
            {
                if (NewMsgEvent != null)
                    NewMsgEvent(ex.Message);
            }

        }
        public void Stop()
        {
            try
            {
                DisconnectClient();
                if (listener != null)
                    listener.Stop();
                if (NewMsgEvent != null)
                    NewMsgEvent("Сервер остановлен!");
            }
            catch (Exception ex)
            {
                if (NewMsgEvent != null)
                    NewMsgEvent(ex.Message);
            }
        }
        public void SendMessage(string msg) => clientObject.SendMessage(msg);
    }
}