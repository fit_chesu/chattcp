﻿using System;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace ChatTcp
{
    class Client
    {
        static NetworkStream stream;
        public delegate void NewMsgAction(string msg);
        public event NewMsgAction NewMsgEvent;
        TcpClient client;
        string IPAddres;
        int PORT;
        bool isWork;

        public Client(string IPAddres, int PORT)
        {
            this.IPAddres = IPAddres;
            this.PORT = PORT;
        }
        public void StartClient()
        {
            Thread clientThread = new Thread(new ThreadStart(ConnectToServer));
            clientThread.Start();
        }
        public void ConnectToServer()
        {
            client = null;
            try
            {
                isWork = true;
                client = new TcpClient(IPAddres, PORT);
                stream = client.GetStream();

                while (isWork)
                {
                    // получаем ответ
                    byte[] data = new byte[64]; // буфер для получаемых данных
                    StringBuilder builder = new StringBuilder();
                    int bytes = 0;
                    do
                    {
                        bytes = stream.Read(data, 0, data.Length);
                        builder.Append(Encoding.Unicode.GetString(data, 0, bytes));
                    }
                    while (stream.DataAvailable);

                    string message = builder.ToString();
                    if (NewMsgEvent != null)
                        NewMsgEvent(message);
                    if (message.Equals("Disconnect!") == true || message == "")
                    {
                        SendMessage("Server closed connection!");
                        isWork = false;
                        break;
                    }
                    //Console.WriteLine("Сервер: {0}", message);
                }
            }
            catch (Exception ex)
            {
                if (NewMsgEvent != null)
                    NewMsgEvent(ex.Message);
            }
            finally
            {
                client.GetStream().Close();
                client.Close();
                client = null;
            }
        }
        public void SendMessage(string msg)
        {
            // преобразуем сообщение в массив байтов
            byte[] data = Encoding.Unicode.GetBytes($"{msg}");
            // отправка сообщения
            stream.Write(data, 0, data.Length);
        }
        public void Disconnect()
        {
            try
            {
                isWork = false;
                SendMessage("Disconnect!");
                if (NewMsgEvent != null)
                    NewMsgEvent("Соединение завершено!");
            }
            catch (Exception ex)
            {
                if (NewMsgEvent != null)
                    NewMsgEvent(ex.Message);
            }
        }
    }
}