﻿using System;
using System.Windows;
using System.Threading;

namespace ChatTcp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        enum Role { server, client};
        Role myRole;
        Server server;
        Client client;
        private const int port = 8888;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnStartServer_Click(object sender, RoutedEventArgs e)
        {
            myRole = Role.server;
            listBox.Items.Add("Server is started!");
            
            try
            {
                server = new Server(port);
                server.NewMsgEvent += RecieveMessage;
                server.StartServer();
            }
            catch (Exception ex)
            {
                server.Stop();
                listBox.Items.Add(ex.Message);
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (server != null)
            {
                server.Stop();
                server = null;
            }
            if (client != null)
            {
                client.Disconnect();
                client = null;
            }
        }

        private void btnSend_Click(object sender, RoutedEventArgs e)
        {
            if (myRole == Role.server && server != null)
            {
                server.SendMessage($"server: {textBox.Text}");
            }
            else if (myRole == Role.client && client != null)
            {
                client.SendMessage($"client: {textBox.Text}");
            }
        }

        private void btnConnectToServer_Click(object sender, RoutedEventArgs e)
        {
            myRole = Role.client;
            listBox.Items.Add("Client is started!");
            client = new Client(tbIPAddress.Text, port);
            client.NewMsgEvent += RecieveMessage;
            client.StartClient();
        }

        private void btnDisconnect_Click(object sender, RoutedEventArgs e)
        {
            if (server != null)
            {
                server.DisconnectClient();
            }
            if (client != null)
            {
                client.Disconnect();
            }
        }

        public void RecieveMessage(string msg)
        {
            this.Dispatcher.Invoke(() =>
            {
                listBox.Items.Add($"{msg}");
            });
        }

        private void btnStopServer_Click(object sender, RoutedEventArgs e)
        {
            if (server != null)
            {
                server.Stop();
            }
        }
    }
}
